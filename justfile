#!/usr/bin/env -S just --justfile

_default:
    @just --list

_deps:
    #!/usr/bin/env bash
    set -euxo pipefail
    if [ ! -x "$(command -v stow)" ]; then
        sudo apt install -y \
        	stow
    fi

stow: _deps
    stow --dotfiles --no-folding --verbose --restow */

unstow: _deps
    stow --dotfiles --no-folding --verbose --delete */

fmt:
    shfmt -f . | xargs -I{} shfmt -i 2 -ci -w {}

lint:
    shfmt -f . | xargs -I{} shellcheck -x {}
