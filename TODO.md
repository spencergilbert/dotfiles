# TODO

A list of things to try and investigate.

## Terminal Emulators

* [ ] [Alacritty](https://github.com/alacritty/alacritty)
* [ ] [Console](https://gitlab.gnome.org/GNOME/console) - _Linux Only_
* [ ] [foot](https://codeberg.org/dnkl/foot) - _Linux, Wayland Only_
* [ ] [Ghostty](https://mitchellh.com/ghostty)
* [ ] [GNOME Terminal](https://gitlab.gnome.org/GNOME/gnome-terminal) - _Linux Only_
* [ ] [Prompt](https://gitlab.gnome.org/chergert/prompt) - _Linux Only_
* [ ] [rio](https://github.com/raphamorim/rio)

## GUI

### Editors

* [ ] [JetBrains .\*](https://www.jetbrains.com/)
* [ ] [VS Code](https://github.com/microsoft/vscode)

## CLI/TUI

### Containers

* [ ] [buildah](https://github.com/containers/buildah)
* [ ] [podman](https://github.com/containers/podman)
* [ ] [skopeo](https://github.com/containers/skopeo)

### Container Toolbox

* [ ] [distrobox](https://github.com/89luca89/distrobox)
* [ ] [Toolbox](https://github.com/containers/toolbox)
  * [ ] [Quay Containerfiles](https://github.com/travier/quay-containerfiles)
  * [ ] [Toolbx Images](https://github.com/toolbx-images/images)
  * [ ] [uBlue](https://github.com/ublue-os/boxkit)

### Editors

* [ ] [Helix](https://github.com/helix-editor/helix)
* [ ] [Neovim](https://github.com/neovim/neovim)

### Miscellaneous

* [ ] [atuin](https://github.com/ellie/atuin)
* [ ] [bat](https://github.com/sharkdp/bat)
* [ ] [bottom](https://github.com/ClementTsang/bottom)
* [ ] [boxxy](https://github.com/queer/boxxy)
* [ ] [btop](https://github.com/aristocratos/btop)
* [ ] [bytehound](https://github.com/koute/bytehound)
* [ ] [Clipboard](https://github.com/Slackadays/Clipboard)
* [ ] [dprint](https://github.com/dprint/dprint)
* [ ] [erdtree](https://github.com/solidiquis/erdtree)
* [ ] [eza](https://github.com/eza-community/eza)
* [ ] [Glow](https://github.com/charmbracelet/glow)
* [ ] [lsd](https://github.com/Peltoche/lsd)
* [ ] [melody](https://github.com/yoav-lavi/melody)
* [ ] [mold](https://github.com/rui314/mold)
* [ ] [nala](https://gitlab.com/volian/nala)
* [ ] [ripgrep](https://github.com/BurntSushi/ripgrep)
* [ ] [sccache](https://github.com/mozilla/sccache)
* [ ] [Soft Serve](https://github.com/charmbracelet/soft-serve)
* [ ] [stow](https://github.com/aspiers/stow)
* [ ] [starship](https://github.com/starship/starship)
* [ ] [tab-rs](https://github.com/austinjones/tab-rs)
* [ ] [topgrade](https://github.com/topgrade-rs/topgrade)
* [ ] [VHS](https://github.com/charmbracelet/vhs)
* [ ] [Wishlist](https://github.com/charmbracelet/wishlist)
* [ ] [xplr](https://github.com/sayanarijit/xplr)
* [ ] [zoxide](https://github.com/ajeetdsouza/zoxide)

### Provider CLIs

* [ ] [berg](https://codeberg.org/RobWalt/codeberg-cli)
* [ ] [gh](https://github.com/cli/cli)
* [ ] [glab](https://gitlab.com/gitlab-org/cli)
* [ ] [hut](https://git.sr.ht/~emersion/hut)

### Task Runner

* [ ] [Dagger](https://github.com/dagger/dagger)
* [ ] [Earthly](https://github.com/earthly/earthly)
* [ ] [haredo](https://sr.ht/~autumnull/haredo/)
* [ ] [just](https://github.com/casey/just)
* [ ] [mise](https://github.com/jdx/mise)
* [ ] [Task](https://github.com/go-task/task)

### Version Control

* [ ] [cocogitto](https://github.com/cocogitto/cocogitto)
* [ ] [difftastic](https://github.com/Wilfred/difftastic)
* [ ] [git-absorb](https://github.com/tummychow/git-absorb)
* [ ] [git-cliff](https://github.com/orhun/git-cliff)
* [ ] [git-branchless](https://github.com/arxanas/git-branchless)
* [ ] [git-delta](https://github.com/dandavison/delta)
* [ ] [git-stack](https://github.com/gitext-rs/git-stack)
* [ ] [git-town](https://github.com/git-town/git-town)
* [ ] [gitoxide](https://github.com/Byron/gitoxide)
* [x] [gitsign](https://github.com/sigstore/gitsign)
  * [ ] [gitsign-credential-cache](https://github.com/sigstore/gitsign/tree/main/cmd/gitsign-credential-cache)
* [ ] [jj](https://github.com/martinvonz/jj)
* [ ] [spl](https://github.com/facebook/sapling)
* [ ] [spr](https://github.com/getcord/spr)
* [ ] [stg](https://github.com/stacked-git/stgit)

### Version Management

* [ ] [asdf-vm](https://github.com/asdf-vm/asdf)
  * [ ] [asdf-direnv](https://github.com/asdf-community/asdf-direnv)
* [ ] [pkgx](https://pkgx.sh/)
* [ ] [mise](https://github.com/jdx/mise)
* [ ] [woof](https://github.com/version-manager/woof)
