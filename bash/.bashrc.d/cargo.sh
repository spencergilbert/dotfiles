#!/usr/bin/env bash

# Rust(up) (see https://rustup.rs/)
[ -d "$HOME"/.cargo/bin ] && export PATH=$HOME/.cargo/bin:$PATH
