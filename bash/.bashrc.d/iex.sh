#!/usr/bin/env bash

# IEx (see https://hexdocs.pm/iex/IEx.html)
if [[ -x "$(command -v iex)" ]]; then
  export ERL_AFLAGS="-kernel shell_history enabled"
fi
