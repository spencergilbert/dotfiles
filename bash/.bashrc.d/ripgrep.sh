#!/usr/bin/env bash

# ripgrep (see https://github.com/BurntSushi/ripgrep)
if [[ -x "$(command -v rg)" ]]; then
  alias grep="rg --engine=auto --no-heading --smart-case"
fi
