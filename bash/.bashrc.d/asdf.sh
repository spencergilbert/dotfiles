#!/usr/bin/env bash

# asdf (see https://asdf-vm.com/)
if [[ -d "$HOME"/.asdf ]]; then
  # shellcheck source=/dev/null
  . "$HOME"/.asdf/asdf.sh
  # shellcheck source=/dev/null
  . "$HOME"/.asdf/completions/asdf.bash
fi
