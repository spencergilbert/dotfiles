#!/usr/bin/env bash

# bat (see https://github.com/helix-editor/helix)
if [[ -x "$(command -v hx)" ]]; then
  export HELIX_RUNTIME="/home/spencer.gilbert/Projects/github.com/helix-editor/helix/runtime"
fi
