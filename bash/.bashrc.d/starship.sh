#!/usr/bin/env bash

# Starship (see https://starship.rs/)
if [[ -x "$(command -v starship)" ]]; then
  eval "$(starship init bash)"
fi
