#!/usr/bin/env bash

# fd (see https://github.com/sharkdp/fd)
if [[ -x "$(command -v fdfind)" ]]; then
  # Executable is `fdfind` when installed via `apt`
  # ln -s $(which fdfind) ~/.local/bin/fd
  alias fd="fdfind"
fi
