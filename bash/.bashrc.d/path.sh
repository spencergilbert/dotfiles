#!/usr/bin/env bash

# User specific environment
if ! [[ $PATH =~ $HOME/.local/bin:$HOME/bin: ]]; then
  PATH=$HOME/.local/bin:$HOME/bin:$PATH

  export PATH
fi
