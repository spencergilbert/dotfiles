#!/usr/bin/env bash

# Bash History - See bash(1) for more options

# Don't put duplicate lines or lines starting with space in the history.
export HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it
shopt -s histappend

# Setting history length
export HISTSIZE=1000
export HISTFILESIZE=2000
