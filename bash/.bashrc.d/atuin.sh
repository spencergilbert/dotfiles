#!/usr/bin/env bash

# atuin (see https://atuin.sh)
# shellcheck disable=SC1090
[[ -f ~/.bash-preexec.sh ]] && source ~/.bash-preexec.sh
eval "$(atuin init bash)"
