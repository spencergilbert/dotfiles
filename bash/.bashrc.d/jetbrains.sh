#!/usr/bin/env bash

# JetBrains Toolbox (see https://www.jetbrains.com/toolbox-app/)
export PATH=$HOME/.local/share/JetBrains/Toolbox/scripts:$PATH

# TODO: This is broken and I'm not sure why
# if [[ -x "$(command -v jetbrains-toolbox)" ]] && ! [[ $PATH =~ $HOME/.local/share/JetBrains/Toolbox/scripts ]]; then
#   export PATH=$HOME/.local/share/JetBrains/Toolbox/scripts:$PATH
# fi
