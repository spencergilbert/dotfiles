#!/usr/bin/env bash

# zoxide (see https://github.com/ajeetdsouza/zoxide)
if [[ -x "$(command -v zoxide)" ]]; then
  eval "$(zoxide init bash)"
fi
