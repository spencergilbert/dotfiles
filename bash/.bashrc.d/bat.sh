#!/usr/bin/env bash

# bat (see https://github.com/sharkdp/bat)
if [[ -x "$(command -v batcat)" ]]; then
  # Executable is `batcat` when installed via `apt`
  # ln -s $(which fdfind) ~/.local/bin/fd
  alias bat="batcat"

  # Preserve default behavior of `cat`
  # alias cat="batcat --paging=never"

  # Use `bat` as a colorizing pager for `man`
  # export MANPAGER="sh -c 'col -bx | bat -l man -p'"
  export MANPAGER="sh -c 'col -bx | batcat -l man -p'"
fi
