#!/usr/bin/env bash

# podman (see https://podman.io/)
if [[ -x "$(command -v podman)" ]]; then
  alias docker="podman"
fi
