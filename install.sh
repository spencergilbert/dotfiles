#!/usr/bin/env bash

if [ ! -d "$HOME"/.dotfiles ]; then
  git clone git@gitlab.com:spencergilbert/dotfiles.git "$HOME"/.dotfiles
fi

ERLANG_PKGS="build-essential autoconf m4 libncurses5-dev libwxgtk3.0-gtk3-dev \
libwxgtk-webview3.0-gtk3-dev libgl1-mesa-dev libglu1-mesa-dev libpng-dev \
libssh-dev unixodbc-dev xsltproc fop libxml2-utils libncurses-dev openjdk-11-jdk"
PHX_PKGS="inotify-tools"
VECTOR_PKGS="clang cmake libssl-dev"
# shellcheck disable=SC2086
sudo apt install -y \
  $ERLANG_PKGS \
  $PHX_PKGS \
  $VECTOR_PKGS \
  bat \
  buildah \
  just \
  mold \
  nala \
  podman \
  podman-toolbox \
  qemu-user-static \
  ripgrep \
  shellcheck \
  shfmt \
  skopeo \
  stow \
  zoxide

# Install applications from Flathub, if `flatpak` is available
if [ -x "$(command -v flatpak)" ]; then
  $(which flatpak) remote-add --user --if-not-exists \
    flathub https://flathub.org/repo/flathub.flatpakrepo

  $(which flatpak) install --or-update -y --noninteractive flathub \
    com.github.geigi.cozy \
    com.discordapp.Discord \
    com.github.johnfactotum.Foliate \
    org.gnome.Solanum
fi

# Install `rustup`, if it's not installed, without editing shell configuration
if [ ! -x "$(command -v rustup)" ]; then
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --no-modify-path -y

  "$HOME"/.cargo/bin/cargo/rustup component add rust-analyzer
fi

# Install `sccache` without any configured `rustc-wrapper` in `.cargo/config.toml`
# libssl-dev
if [ ! -x "$(command -v sccache)" ]; then
  "$HOME"/.cargo/bin/cargo install --locked --config build.rustc-wrapper=\"\" sccache
fi

# Install crates
# TODO: Including `sccache` here always reinstalled it, not sure why
# `gitoxide`, `starship`:`cmake`
"$HOME"/.cargo/bin/cargo install --locked \
  bottom \
  difftastic \
  git-delta \
  gitoxide \
  lsd \
  starship

# Install `asdf`, if it's not installed
if [ ! -d "$HOME"/.asdf ]; then
  git clone https://github.com/asdf-vm/asdf.git "$HOME"/.asdf --branch v0.12.0
fi

# Install `asdf` plugins
PLUGINS=(
  "elixir https://github.com/asdf-vm/asdf-elixir.git"
  "erlang https://github.com/asdf-vm/asdf-erlang.git"
  #"dprint https://github.com/asdf-community/asdf-dprint.git"
  "gitsign https://github.com/spencergilbert/asdf-gitsign.git"
  "nodejs https://github.com/asdf-vm/asdf-nodejs.git"
  #"vector https://github.com/spencergilbert/asdf-vector.git"
)
for plugin in "${PLUGINS[@]}"; do
  # TODO: There's probably a way of doing this without disabling the check
  # shellcheck disable=SC2086
  "$HOME"/.asdf/bin/asdf plugin-add $plugin
done

if [ -x "$(command -v just)" ]; then
  # Can't `stow` over these existing files
  # These may not exist, the script should handle it
  mv "$HOME"/.bash_logout /tmp/.bash_logout.bk
  mv "$HOME"/.bashrc /tmp/.bashrc.bk
  mv "$HOME"/.profile /tmp/.profile.bk
  just stow
fi

# Install JetBrains Toolbox
# curl -fsSL https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash

systemctl mask --now apt-news.service
systemctl mask --now esm-cache.service
systemctl mask --now fwupd-refresh.timer
