# btrfs & Pop!_OS

```bash
sudo -i
cryptsetup luksOpen /dev/nvme1n1p1 cryptdata1
pv create /dev/mapper/cryptdata1
vgextend data /dev/mapper/cryptdata1
lvextend /dev/data/root /dev/mapper/cryptdata1
btrfs filesystem resize max /
```

# LUKS2

- [Unlocking LUKS2](https://0pointer.net/blog/unlocking-luks2-volumes-with-tpm2-fido2-pkcs11-security-hardware-on-systemd-248.html)
